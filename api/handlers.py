"""
Handlers
"""
import aioredis
from aiohttp import web
import logging
import sys


async def database(request):
    try:

        merge = request.rel_url.query['merge']

        data = await request.json()

        redis = await aioredis.create_redis(
            'redis://localhost')

        if merge == 0:
            redis.delete(data['name'])
        elif merge == 1:
            await redis.set(data['name'], data['value'])

        redis.close()
        await redis.wait_closed()

        return web.json_response('Success')

    except BaseException as ex:

        logging.error(str(sys.exc_info()))
        logging.error(ex)


async def convert(request):

    try:
        currency_from = request.rel_url.query['from']

        currency_to = request.rel_url.query['to']

        amount = float(request.rel_url.query['amount'])

        result = currency_from + "_" + currency_to

        redis = await aioredis.create_redis(
            'redis://localhost')

        result = await redis.get(result)

        result = float(result.decode())

        total = amount * result

        redis.close()
        await redis.wait_closed()

        return web.json_response({'Total' + '_' + currency_to: total})

    except BaseException as ex:

        logging.error(str(sys.exc_info()))
        logging.error(ex)
