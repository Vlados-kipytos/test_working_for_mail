"""
Routes
"""
from api.handlers import *


def setup_routes(app):
    app.add_routes([
        web.post('/database', database),
        web.get('/convert', convert)])
