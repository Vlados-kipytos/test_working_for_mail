"""
Main
"""
from api.routes import *


def main():
    app = web.Application()
    setup_routes(app)
    web.run_app(app)


if __name__ == '__main__':
    main()
